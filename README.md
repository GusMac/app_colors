Introducción.

El equipo de dedesarrollo de Chedraui haciendo uso de tecnologías como VueJs y Tailwind como framework ha diseñado y desarrollado con ayuda de de una API una aplicación de colores para que los demás departamentos y diseñadores puedan tener acceso a los colores que se van a utilizar para todos los productos; páginas web, nuevos diseños, flyers y hasta para redes sociales.


Tecnologías.

Se dició usar VueJS como marco progresivo para la construcción del IU ya que cuenta con herramientas modernas y bibliotecas de soporte. Para esta aplicación, su desarrollo se facilita con las distintas herramientas que se utilizaron, entre ellas Taildwind como marco CSS, ya que viene con todo tipo de componentes siendo efectivo en nuestra app. De igual manera se pensó utilizar VueJS y demás tecnologías pensando en el desarrollo de herramientas futuras que Chedraui necesite.

- VueJS
- Taildwind
- Axios (consumir API)


Implementación.

- 1) Dirigase a https://gitlab.com/GusMac/app_colors
- 2) Clone el proyecto a su computadora.
- 3) Abra su aplicación de consola favorita.
- 4) Navegue a la carpeta e instale paquetes usando el siguiente comando: npm install
- 5) Para ejecutar el proyecto a nivel local: npm run serve
- 6) Abra http://localhost:8080 para verlo en el navegador.


Netlifi.

Para una rápida visualización, prueba e implementación de la app, se ha subido a una cuenta de Netlify. Solo debes dirigirte a la siguiente ruta:

- https://app-colors-front-end-developer.netlify.app/