export default [
  {
    url: "/",
    name: "App Colors",
    slug: "app",
    icon: "DropletIcon",
  },
  {
    url: "/readme",
    name: "Readme",
    slug: "readme",
    icon: "FileIcon",
  },
]
