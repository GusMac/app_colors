export default {
  pages: {
    key: "title",
    data: [
      {title: 'App Colors',   url: '/', icon: 'DropletIcon', is_bookmarked: false},
      {title: 'Readme', url: '/readme', icon: 'FileIcon', is_bookmarked: false},
    ]
  }
}
